# rules

Business and Operational Rules for operating Crossref Systems

- blocked-orcid-claims: occasionally a researcher will ask us to *stop* autoupdate from being triggered for a specific DOI. This is where we record these requests so that they are honored by the ORCID auto-update feature. 
- reserved-ids: occasionally a reseracher will ask us to block autoupdate for their ORCID altogether. This is where we record those requests. We also sometimes include IDs here that we use for testing.
- gem-country-code: this is the list of locations that are elligable for our Global Equitable Membership (GEM) program.
